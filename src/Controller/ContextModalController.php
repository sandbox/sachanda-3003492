<?php

namespace Drupal\context_popup\Controller;

use Drupal\block\BlockViewBuilder;
use Drupal\block\Entity\Block;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * CustomModalController class to load modal block.
 *
 * @category Class
 * @package CGI
 * @author "Santosh Chandanlar <santosh.chandankar@cgi.com>"
 * @license http://www.cgi.com
 * @link http://local8.cgi.com/admin/structure/context
 */
class ContextModalController extends ControllerBase {

  /**
   * Entity type class object manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManger;

  /**
   * Renderer Interface.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
        RendererInterface $renderer
    ) {
    $this->entityTypeManger = $entityTypeManager;
    $this->renderer = $renderer;
  }

  /**
   * Get Block HTML to display in modal.
   *
   * @param string $bid
   *   Block id to render.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   Rendered HTML for the block.
   */
  public function getBlockHtml($bid) {
    $response = new Ajaxresponse();

    $output = '<div id="cgi-context-modal-webform">';
    $options = [
      'dialogClass' => 'popup-dialog-class',
      'width' => '50%',
    ];
    $block_manager = \Drupal::service('plugin.manager.block');
    $config = [];

    if ($block = $block_manager->createInstance($bid, $config)) {
      // You can hard code configuration or you load from settings.
      // Some blocks might implement access check.
      // $access_result = $plugin_block->access(\Drupal::currentUser());
      // Return empty render array if user doesn't have access.
      // $access_result can be boolean or an AccessResult class
      /*
       * if (is_object($access_result) && $access_result->isForbidden() ||
       * is_bool($access_result) && ! $access_result) {
       * // You might need to add some cache tags/contexts.
       * return [];
       * }
       */
      $render = $block->build();
      // In some cases, you need to add the cache tags/context depending on
      // the block implemention. As it's possible to add the cache tags and
      // contexts in the render method and in ::getCacheTags and
      // ::getCacheContexts methods.
    }
    elseif (empty($block = BlockContent::load($bid))) {
      $render = \Drupal::entityTypeManager()->getViewBuilder('block_content')->view(
        $block
      );
    }
    elseif ($block = Block::load($bid)) {
      $render = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block);
    }
    else {
      $render = BlockViewBuilder::lazyBuilder('bartik_search', 'full');
    }
    $output .= \Drupal::service('renderer')->renderRoot($render);

    $output .= '</div>';
    $response->addCommand(
        new OpenModalDialogCommand(t('Contact us Modal'), $output, $options)
    );

    return $response;
  }

  /**
   * Display Block in Modal Dialog.
   */
  public function modal() {
    $param = \Drupal::request()->query->all();
    /* return block html to modal */
    return $this->getBlockHtml($param['id']);
  }

}
