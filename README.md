Context Pop-up module
================================================================

This module defines a plugin type of Context Reaction that provides Context Reaction "Popup link".

This Reaction facilates funcionality to place/render a link which opens the referenced page/block/form in modal dialog(Pop-up).

How to use ?
-----------------------------------------------------------------------
Follow below instructions to add popup link at desired region:

1. Add Context from using Context Menuitem from Structure Menu.
2. Add Required Conditions
3. Select Reaction "Popup Link"
4. Select theme to which this link needs to display.
5. Add Link title
6. Add Link Description (tooltip for the link)
7. Add URL of the page/form iff you want to display page/form in modal dialog on click of link and select region from "link region".
 OR
  to select Block/form, click on "Place block" button  and select block/form from the available options.
8. Save the Context.


Notes
-----------------------------------------------------------------------

Please note that only Context aware blocks/forms can be selected using "Place block" button. As of now only one popup link can be added per context.

